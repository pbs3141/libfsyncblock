#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>
#include <fcntl.h>
#include <stdarg.h>
#include <errno.h>

int fsync(int fd)
{
    if (fcntl(fd, F_GETFD) == -1) return -1;
    errno = 0;
    return 0;
}

void sync(void)
{
}

int fdatasync(int fd)
{
    if (fcntl(fd, F_GETFD) == -1) return -1;
    errno = 0;
    return 0;
}

int msync(void *addr, size_t length, int flags)
{
    errno = 0;
    return 0;
}

int sync_file_range(int fd, off64_t offset, off64_t nbytes, unsigned int flags)
{
    if (fcntl(fd, F_GETFD) == -1) return -1;
    errno = 0;
    return 0;
}

int open(const char *file, int oflag, ...)
{
    int mode = 0;
    if (__OPEN_NEEDS_MODE(oflag))
    {
        va_list arg;
        va_start(arg, oflag);
        mode = va_arg(arg, int);
        va_end(arg);
    }
    
    oflag &= ~(O_SYNC | O_DSYNC);
    
    return syscall(SYS_openat, AT_FDCWD, file, oflag, mode);
}

int open64(const char *file, int oflag, ...)
{
    int mode = 0;
    if (__OPEN_NEEDS_MODE(oflag))
    {
        va_list arg;
        va_start(arg, oflag);
        mode = va_arg(arg, int);
        va_end(arg);
    }
    
    oflag &= ~(O_SYNC | O_DSYNC);

    return syscall(SYS_openat, AT_FDCWD, file, oflag, mode);
}
