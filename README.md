# libfsyncblock

LD_PRELOAD library to disable filesystem synchronisation.

## Description

This project is a fork of [libeatmydata](https://github.com/stewartsmith/libeatmydata). Like libeatmydata, it turns all filesystem-synchronisation system calls into no-ops, sacrificing data integrity at the altar of performance and disk longevity. The main difference is that it uses a different mechanism to wrap system calls, avoiding potential incompatibilities (historically, with Google Chrome). It also has virtually no dependencies.

## Usage

* To disable file-system synchronisation for a single command, run

      LD_PRELOAD=/usr/lib/libfsyncblock.so command
    
* To disable it system-wide, add the line

      /usr/lib/libfsyncblock.so
   
  to /etc/ld.so.preload.
  
  *Note:* A file-system sync can still be performed manually using the [SysRq key](https://wiki.archlinux.org/title/Keyboard_shortcuts#Kernel_(SysRq)).

⚠️ Make sure you understand the risks of disabling file-system synchronisation **before doing either of these things**. Applications which rely on correct behaviour of `fsync` to maintain their integrity will possibly have their state corrupted in the event of a sudden system crash or power-loss event. (That said, the author has been running the second option above for years on several desktop systems without problems, even after a system crash, partly thanks to judicious use of the SysRq key.)

## FAQ

**Q: How do I check it's working?**  
**A:** Run `ldd /usr/bin/ls` and see if libfsyncblock appears in the output.

**Q. Are there any distro packages?**  
**A:** Available [on the AUR](https://aur.archlinux.org/packages/libfsyncblock).

**Q: `fsync` exists for a reason. Disabling it is a bad idea.**  
**A:** The author takes the point of view that `fsync` exists to provide applications with the ability to guarantee consistency of their data after a crash when there is no better way to do it. However, its ease of use means that it has come to be abused by application developers even when there is a better way to do it. As a result, too many applications now perform unnecessary aggressive file-system syncing, actively harming the performance and lifespan of many users' disks for no real benefit. Even in cases where the application developer is right to sync, ultimately the choice should rest with the *user* whether the data being synced is important enough to warrant  the additional overhead. While this is true on servers, on desktops it is usually not the case. For example, most people don't manually sync the disk every time they save a document or photo. So why should the disk be synced every time, say, a new history entry is written to Google Chrome's history database?

**Q: Does blocking `fsync` increase the risk of file-system corruption?**  
**A:** Not on `btrfs` and `zfs`, which guarantee on-disk consistency after a crash, assuming no hardware errors. The only risk is that certain files may revert to their versions before the crash. For some applications, like databases, this may result in state corruption if not all files are reverted in the same way. Typically this is only a serious concern on say, a production server, not a desktop. Other filesystems may be different, but if this wasn't a concern for your primary data, it shouldn't be a concern for config files and databases either.
